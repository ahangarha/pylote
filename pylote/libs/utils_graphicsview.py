# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------
# This is a part of Pylote project.
# Author:       Pascal Peter
# Copyright:    (C) 2008-2019 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------


"""
DESCRIPTION :
    La zone d'affichage (GraphicsView).
    Affiche l'image de fond et les items.
"""

# importation des modules utiles :
from __future__ import division, print_function
import math

import utils, utils_functions, utils_dialogs, utils_instruments

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
    from PyQt5 import QtSvg
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
    from PyQt4 import QtSvg



###########################################################"
#   FONCTIONS GÉOMÉTRIQUES
###########################################################"

def distance(A, B):
    return math.hypot((A.x() - B.x()), (A.y() - B.y()))

def rotation(P, centre, angle):
    x = centre.x() + math.cos(angle) * (P.x() - centre.x()) 
    x += - math.sin(angle) * (P.y() - centre.y())
    y = centre.y() + math.sin(angle) * (P.x() - centre.x()) 
    y += math.cos(angle) * (P.y() - centre.y())
    return QtCore.QPointF(x, y)

def projectionOrthogonale(P, A, B):
    """
    projection orthogonale sur la droite (AB)
    """
    if B.x() == A.x():
      angle = math.pi / 2
    else:
      angle = math.atan((B.y() - A.y()) / (B.x() - A.x()))
    P2 = rotation(P, A, - angle)
    PH2 = QtCore.QPointF(P2.x(), A.y())
    PH1 = rotation(PH2, A, angle)
    return PH1

def distanceSegment(P, A, B):
    """
    distance entre le point P et le segment [AB]
    """
    result = distance(P, A)
    distance_B = distance(P, B)
    if distance_B < result:
        result = distance_B
    H = projectionOrthogonale(P, A, B)
    if distance(H, A) + distance(H, B) <= distance(A, B):
        distance_H = distance(P, H)
        if distance_H < result:
            result = distance_H
    return result



###########################################################"
#   LE GRAPHICSVIEW : ZONE D'AFFICHAGE
###########################################################"

class GraphicsView(QtWidgets.QGraphicsView):
    def __init__(self, parent):
        QtWidgets.QGraphicsView.__init__(self, parent)
        
        # on récupère main (fenêtre principale)
        self.main = parent
        # pas d'ascenseurs
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        scene = QtWidgets.QGraphicsScene(self)
        scene.setSceneRect(0, 0, 1, 1)
        # je n'ai pas compris pourquoi mais il faut créer puis supprimer 
        # un premier item pour que mouseMoveEvent fonctionne dès le début
        # (nécessaire si on utilise le faux curseur) :
        item = QtWidgets.QGraphicsTextItem()
        scene.addItem(item)
        scene.removeItem(item)
        del item
        self.setScene(scene)

        # les éléments de dessin utiles :
        self.drawPen = QtGui.QPen(
            QtCore.Qt.black, 
            1,
            QtCore.Qt.SolidLine, 
            QtCore.Qt.RoundCap,
            QtCore.Qt.RoundJoin)
        self.path = QtGui.QPainterPath()
        self.font = QtGui.QFont()
        self.fontColor = QtGui.QColor()
        self.drawId = -1

        # les différents curseurs affichés sur les objets :
        self.CURSORS = {
            'NOTHING': QtCore.Qt.BlankCursor, 
            'ARROW': QtCore.Qt.ArrowCursor, 
            'CROSS': QtCore.Qt.CrossCursor, 
            'MOVE': QtGui.QCursor(
                utils.doIcon('transform-move', what='PIXMAP')), 
            'ROTATE': QtGui.QCursor(
                utils.doIcon('transform-rotate', what='PIXMAP')), 
            'SCALE': QtGui.QCursor(
                utils.doIcon('transform-scale', what='PIXMAP')), 
            'TRACE': QtGui.QCursor(
                utils.doIcon('transform-trace', what='PIXMAP'), 0, 32), 
            'next': {
                'ARROW': 'MOVE', 
                'MOVE': 'ROTATE', 
                'ROTATE': 'SCALE', 
                'TRACE': 'SCALE', 
                'SCALE': 'MOVE', 
                }, 
            }
        # état de la sélection (pour gérer les actions à effectuer) :
        self.state = {
            'mouseEvent': '', 
            'mouseAction': '', 
            'mouseButton': QtCore.Qt.NoButton, 
            'mousePos': None, 
            'selected': None, 
            'cursor': 'ARROW', 
            'transform': '', 
            'instrumentName': '', 
            'locked': False, 
            'attachInstrument': None, 
            'lastPoint': QtCore.QPointF(), 
            }
        self.changeCursor()

    def changeCursor(self, newCursor='ARROW'):
        if utils_instruments.WITH_FALSE_CURSOR:
            newCursor = 'NOTHING'
        self.setCursor(self.CURSORS[newCursor])
        self.viewport().setCursor(self.CURSORS[newCursor])



    def mousePressEvent(self, event):
        """
        On veut distinguer le clic du double-clic et du tracé (mouseMove).
        Ici, on met les variables mouseRelease et doubleClick à False,
        et on retarde la gestion du simple clic à la fonction doMouseClick.
        Entre temps, si c'est un double-clic, la variable 
        doubleClick sera devenue True
        et si un événement mouseRelease a eu lieu, 
        mouseRelease sera devenue True.
        Enfin firstMouseMove sert à initialiser un événement mouseMove.
        """
        super(GraphicsView, self).mousePressEvent(event)
        self.state['mouseEvent'] = 'mousePress'
        self.state['mouseButton'] = event.button()
        self.state['mousePos'] = self.mapToScene(event.pos())

        if len(self.scene().selectedItems()) > 0:
            selected = self.scene().selectedItems()[0]
            isInstrument = isinstance(
                selected, utils_instruments.Instrument)
            if isInstrument:
                self.state['instrumentName'] = selected.objectName()
            else:
                self.state['instrumentName'] = ''
            if isInstrument or (self.main.drawingMode == 'SELECT'):
                self.state['mouseAction'] = 'editing'
                if selected != self.state['selected']:
                    if self.state['selected'] != None:
                        self.state['selected'].setCursor(self.CURSORS['MOVE'])
                        self.state['selected'].unsetCursor()
                    self.state['selected'] = selected

                    if selected != None:
                        utils.doFlags(selected, 'movable')
                        self.state['transform'] = 'MOVE'
                        selected.setCursor(self.CURSORS['MOVE'])
                        self.state['cursor'] = 'MOVE'
                        self.state['mouseAction'] = 'selecting'
                        if isInstrument:
                            selected.updateZValue()
                if self.state['mouseButton'] == QtCore.Qt.RightButton:
                    self.state['transform'] = 'ROTATE'
                    if (self.state['instrumentName'] == 'Compass') \
                        and (utils_instruments.IS_TRACING_ENABLED):
                            #selected.setCursor(self.CURSORS['TRACE'])
                            selected.setCursor(self.CURSORS['ROTATE'])
                    else:
                        selected.setCursor(self.CURSORS['ROTATE'])
                    self.state['cursor'] = 'ROTATE'
                elif self.state['mouseButton'] == QtCore.Qt.MiddleButton:
                    self.state['transform'] = 'SCALE'
                    selected.setCursor(self.CURSORS['SCALE'])
                    self.state['cursor'] = 'SCALE'
            else:
                self.state['transform'] = ''
        elif self.main.drawingMode == 'SELECT':
            self.state['instrumentName'] = ''
            self.state['transform'] = ''
            if self.state['selected'] != None:
                self.state['selected'].setCursor(self.CURSORS['MOVE'])
                self.state['selected'].unsetCursor()
                self.state['selected'] = None
        elif self.main.drawingMode != 'NO':
            self.state['instrumentName'] = ''
            self.state['transform'] = ''
            self.state['mouseAction'] = 'drawing'
            if self.state['selected'] != None:
                self.state['selected'].setCursor(self.CURSORS['MOVE'])
                self.state['selected'].unsetCursor()
                self.state['selected'] = None
        # en dessous de 300, ça foire :
        QtCore.QTimer.singleShot(300, self.doMouseClick)

    def mouseDoubleClickEvent(self, event):
        """
        On met la variable doubleClick à True, pour ne pas executer 
        la procédure liée au simple clic.
        Si le double-clic est fait sur un texte ou le label d'un point,
        il permet de l'éditer.
        Sinon, il permet de cacher/afficher la barre d'outils.
        """
        super(GraphicsView, self).mouseDoubleClickEvent(event)
        self.state['mouseEvent'] = 'mouseDoubleClick'
        if self.state['instrumentName'] == 'Compass':
            utils_instruments.changeTracingEnabled()
            if utils_instruments.IS_TRACING_ENABLED:
                self.main.compass.setOpacity(1)
            else:
                self.main.compass.setOpacity(0.5)
        elif self.main.editText() == False:
            if not(self.main.toolsWindow.toolsKidMode):
                if self.main.toolsWindow.isVisible():
                    self.main.doMinimizeToolsWindow()
                else:
                    self.main.doRestoreToolsWindow()

    def mouseReleaseEvent(self, event):
        """
        On termine l'éventuel tracé
        """
        super(GraphicsView, self).mouseReleaseEvent(event)
        if self.state['mouseEvent'] == 'mouseDoubleClick':
            return
        self.state['mouseEvent'] = 'mouseRelease'

        if self.state['mouseAction'] == 'editing':
            self.doTransform()
        elif self.state['mouseAction'] == 'scribbling':
            point = self.mapToScene(event.pos())
            self.doDrawing(point)
        elif self.state['mouseAction'] == 'selecting':
            self.main.saveTempFile()

        self.state['transform'] = ''
        #self.state['instrumentName'] = ''
        self.state['mouseButton'] = QtCore.Qt.NoButton

    def doMouseClick(self):
        """
        On vérifie d'abord que c'est bien un simple clic
        (variables doubleClick et mouseRelease).
        Gestion des différents cas possibles 
        (ajout d'un texte, début d'un tracé, ...)
        """
        if self.state['mouseEvent'] == 'mouseRelease':
            self.state['mouseEvent'] = 'mouseClick'
        else:
            return

        if self.state['selected'] != None:
            if self.state['mouseAction'] == 'selecting':
                if self.state['cursor'] in ('MOVE', 'ROTATE', 'SCALE'):
                    self.state['transform'] = self.state['cursor']
                elif self.state['cursor'] == 'TRACE':
                    self.state['transform'] = 'ROTATE'
                if (self.state['instrumentName'] == '') \
                    and (self.state['transform'] == 'MOVE'):
                        utils.doFlags(self.state['selected'], 'movable')
                else:
                    utils.doFlags(self.state['selected'], 'selectable')
            elif self.state['mouseAction'] == 'editing':
                self.state['cursor'] = self.CURSORS['next'][self.state['cursor']]
                if self.state['cursor'] in ('MOVE', 'ROTATE', 'SCALE'):
                    self.state['transform'] = self.state['cursor']
                if (self.state['transform'] == 'ROTATE') \
                    and (self.state['instrumentName'] == 'Compass') \
                    and (utils_instruments.IS_TRACING_ENABLED):
                        #self.state['selected'].setCursor(self.CURSORS['TRACE'])
                        self.state['selected'].setCursor(self.CURSORS['ROTATE'])
                else:
                    self.state['selected'].setCursor(
                        self.CURSORS[self.state['cursor']])
                if (self.state['instrumentName'] == '') \
                    and (self.state['transform'] == 'MOVE'):
                        utils.doFlags(self.state['selected'], 'movable')
                elif (self.state['instrumentName'] != '') \
                    and (self.state['transform'] == 'MOVE'):
                        utils.doFlags(self.state['selected'], 'movable')
                else:
                    utils.doFlags(self.state['selected'], 'selectable')
        elif self.state['mouseAction'] == 'drawing':
            point = self.state['mousePos']
            self.doDrawing(point)

    def mouseMoveEvent(self, event):
        """
        D'une part, si on trace une courbe, on met à jour.
        D'autre part, on vérifie quel curseur est affiché 
        (y compris le faux)
        """
        super(GraphicsView, self).mouseMoveEvent(event)
        if self.state['mouseEvent'] not in ('firstMouseMove', 'mouseMove'):
            self.state['mouseEvent'] = 'firstMouseMove'
        elif self.state['mouseEvent'] != 'mouseMove':
            self.state['mouseEvent'] = 'mouseMove'

        if utils_instruments.WITH_FALSE_CURSOR:
            point = self.mapToScene(event.pos())
            self.main.myCursor.setPos(point.x(), point.y())

        if self.state['mouseEvent'] == 'firstMouseMove':
            # pour ne calculer qu'une seule fois :
            if self.state['mouseAction'] == 'editing':
                if self.state['cursor'] in ('MOVE', 'ROTATE', 'SCALE'):
                    self.state['transform'] = self.state['cursor']
                elif self.state['cursor'] == 'TRACE':
                    self.state['transform'] = 'ROTATE'
                else:
                    self.state['transform'] = ''
                if (self.state['selected'] != None) \
                    and (self.state['mouseButton'] != QtCore.Qt.NoButton):
                        point = self.state['selected'].mapFromScene(
                            self.state['mousePos'])
                        self.doTransform(point)
            elif self.state['mouseAction'] == 'drawing':
                point = self.state['mousePos']
                self.doDrawing(point)
            else:
                self.state['transform'] = ''
            self.state['mouseEvent'] = 'mouseMove'
        elif self.state['mouseEvent'] == 'mouseMove':
            if self.state['mouseAction'] == 'editing':
                if (self.state['selected'] != None) \
                    and (self.state['mouseButton'] != QtCore.Qt.NoButton):
                        point = self.state['selected'].mapFromScene(
                            self.mapToScene(event.pos()))
                        self.doTransform(point)
            elif self.state['mouseAction'] == 'scribbling':
                if self.state['mouseButton'] != QtCore.Qt.NoButton:
                    point = self.mapToScene(event.pos())
                    self.doDrawing(point)



    def doTransform(self, point=None):
        mouseEvent = self.state['mouseEvent']
        if mouseEvent == 'firstMouseMove':
            center = self.state['selected'].transformOriginPoint()
            rayon = QtCore.QLineF(center, point)
            self.initialLength = rayon.length()
            if self.initialLength < 1:
                self.initialLength = 1
            self.initialAngle = rayon.angle()
            # cas du tracé au compas :
            if (self.state['transform'] == 'ROTATE') \
                and (self.state['instrumentName'] == 'Compass') \
                and (utils_instruments.IS_TRACING_ENABLED):
                    self.last = None
                    center = self.state['selected'].mapToScene(center)
                    point = self.state['selected'].mapToScene(
                        self.state['selected'].tracePoint)
                    self.initArc(center, point)
                    self.state['selected'].isTracing = True
        elif mouseEvent == 'mouseMove':
            if (self.state['instrumentName'] != '') and self.state['locked']:
                return
            center = self.state['selected'].transformOriginPoint()
            rayon = QtCore.QLineF(center, point)
            length = rayon.length()
            if length < 1:
                length = 1
            angle = rayon.angle()
            if self.state['transform'] == 'ROTATE':
                self.state['selected'].setRotation(
                    self.state['selected'].rotation() 
                    + self.initialAngle - angle)
                # cas du tracé au compas :
                if (self.state['instrumentName'] == 'Compass'):
                    if self.state['selected'].isTracing:
                        self.last = None
                        center = self.state['selected'].mapToScene(center)
                        point = self.state['selected'].mapToScene(
                            self.state['selected'].tracePoint)
                        self.drawArcTo(center, point)
                        if not utils_instruments.IS_TRACING_ENABLED:
                            self.state['selected'].isTracing = False
                    elif utils_instruments.IS_TRACING_ENABLED:
                        self.last = None
                        center = self.state['selected'].mapToScene(center)
                        point = self.state['selected'].mapToScene(
                            self.state['selected'].tracePoint)
                        self.initArc(center, point)
                        self.state['selected'].isTracing = True
            elif self.state['transform'] == 'SCALE':
                mustDo = True
                if self.state['instrumentName'] != '':
                    if self.state['selected'].units \
                        and self.main.toolsWindow.actionUnitsLock.isChecked():
                            mustDo = False
                if mustDo:
                    coeff = length / self.initialLength
                    self.state['selected'].setScale(
                        self.state['selected'].scale() * coeff)
        elif mouseEvent == 'mouseRelease':
            # cas du tracé au compas :
            if (self.state['transform'] == 'ROTATE') \
                and (self.state['instrumentName'] == 'Compass') \
                and (utils_instruments.IS_TRACING_ENABLED):
                    self.last = None
                    center = self.state['selected'].mapToScene(
                        self.state['selected'].transformOriginPoint())
                    point = self.state['selected'].mapToScene(
                        self.state['selected'].tracePoint)
                    item = self.drawArcTo(center, point)
                    self.state['selected'].isTracing = False
                    self.drawId += 1
                    item.setData(QtCore.Qt.UserRole, self.drawId)
                    item.setTransformOriginPoint(item.boundingRect().center())
            # si on avait transformé l'objet avec le bouton droit 
            # ou la molette, on remet l'état en "move" :
            if self.state['mouseButton'] in (
                QtCore.Qt.RightButton, QtCore.Qt.MiddleButton):
                if self.state['selected'] != None:
                    self.state['selected'].setCursor(self.CURSORS['MOVE'])
                    self.state['cursor'] = 'MOVE'
                    self.state['transform'] = 'MOVE'
            self.main.saveTempFile()

    def doDrawing(self, point):
        mouseEvent = self.state['mouseEvent']
        if mouseEvent == 'mouseClick':
            self.state['attachInstrument'] = None
            if self.main.drawingMode == 'POINT':
                for instrument in self.main.listeInstruments[:3]:
                    if (self.state['attachInstrument'] == None) \
                        and instrument.isVisible():
                            A = instrument.mapToScene(instrument.A)
                            B = instrument.mapToScene(instrument.B)
                            d = distanceSegment(point, A, B)
                            attachDistance = self.main.configDict[
                                'MAIN']['attachDistance']
                            if d < attachDistance:
                                self.state['attachInstrument'] = instrument
                                point = projectionOrthogonale(point, A, B)
            ok = False
            toolsWindowHided = False
            try:
                if self.main.drawingMode == 'TEXT':
                    toolsWindowHided = True
                    self.main.toolsWindow.hide()
                    item = QtWidgets.QGraphicsTextItem()
                    item.setFont(self.font)
                    item.setDefaultTextColor(self.fontColor)
                    dialog = utils_dialogs.TextItemDlg(
                        parent=self.main, 
                        graphicsTextItem=item)
                    if dialog.exec_() == QtWidgets.QDialog.Accepted:
                        ok = True
                elif self.main.drawingMode == 'POINT':
                    toolsWindowHided = True
                    self.main.toolsWindow.hide()
                    brush = QtGui.QBrush(self.drawPen.color())
                    exStyle = self.drawPen.style()
                    self.drawPen.setStyle(QtCore.Qt.SolidLine)
                    item = utils_instruments.PointItem(
                        self.main, self.drawPen, brush, self.font)
                    self.drawPen.setStyle(exStyle)
                    ok = item.chooseText(self.drawPen, brush, self.font)
                elif self.main.drawingMode == 'PIXMAP':
                    toolsWindowHided = True
                    self.main.toolsWindow.hide()
                    imagesFormats = utils_functions.u('{0} ({1})').format(
                        QtWidgets.QApplication.translate('main', 'Image Files'), 
                        utils.SUPPORTED_IMAGE_FORMATS_TEXT)
                    fileName = QtWidgets.QFileDialog.getOpenFileName(
                        self.main, 
                        QtWidgets.QApplication.translate('main', 'Open Image'),
                        self.main.pixmapDir, 
                        imagesFormats)
                    if isinstance(fileName, tuple):
                        fileName = fileName[0]
                    if fileName != '':
                        self.main.pixmapDir = QtCore.QFileInfo(
                            fileName).absolutePath()
                        image = QtGui.QImage(fileName)
                        pixmap = QtGui.QPixmap.fromImage(image)
                        item = QtWidgets.QGraphicsPixmapItem(pixmap)
                        ok = True
            finally:
                if ok:
                    self.scene().addItem(item)
                    item.setPos(point)
                    self.drawId += 1
                    item.setData(QtCore.Qt.UserRole, self.drawId)
                    item.setTransformOriginPoint(
                        item.boundingRect().center())
                    self.main.saveTempFile()
                if toolsWindowHided:
                    self.main.toolsWindow.show()
        elif self.main.drawingMode not in ('POINT', 'TEXT', 'PIXMAP'):
            if mouseEvent == 'firstMouseMove':
                self.state['attachInstrument'] = None
                if self.main.drawingMode == 'LINE':
                    for instrument in self.main.listeInstruments[:3]:
                        if (self.state['attachInstrument'] == None) \
                            and instrument.isVisible():
                                A = instrument.mapToScene(instrument.A)
                                B = instrument.mapToScene(instrument.B)
                                d = distanceSegment(point, A, B)
                                attachDistance = self.main.configDict[
                                    'MAIN']['attachDistance']
                                if d < attachDistance:
                                    self.state['attachInstrument'] = instrument
                                    point = projectionOrthogonale(point, A, B)
                self.state['mouseAction'] = 'scribbling'
                self.last = None
                if self.main.drawingMode == 'LINE':
                    self.initLine(point)
                else: 
                    self.initCurve(point)
            elif mouseEvent == 'mouseMove':
                if self.main.drawingMode == 'LINE':
                    if self.state['attachInstrument'] != None:
                        A = self.state['attachInstrument'].mapToScene(
                            self.state['attachInstrument'].A)
                        B = self.state['attachInstrument'].mapToScene(
                            self.state['attachInstrument'].B)
                        H = projectionOrthogonale(point, A, B)
                        self.drawLineTo(H)
                    else:
                        self.drawLineTo(point)
                else:
                    self.drawCurveTo(point)
            elif mouseEvent == 'mouseRelease':
                if self.main.drawingMode == 'LINE':
                    if self.state['attachInstrument'] != None:
                        A = self.state['attachInstrument'].mapToScene(
                            self.state['attachInstrument'].A)
                        B = self.state['attachInstrument'].mapToScene(
                            self.state['attachInstrument'].B)
                        H = projectionOrthogonale(point, A, B)
                        item = self.drawLineTo(H)
                    else:
                        item = self.drawLineTo(point)
                else:
                    item = self.drawCurveTo(point, endLine=True)
                self.drawId += 1
                item.setData(QtCore.Qt.UserRole, self.drawId)
                item.setTransformOriginPoint(item.boundingRect().center())
                self.state['mouseAction'] = ''
                self.state['attachInstrument'] = None
                self.state['lastPoint'] = QtCore.QPointF()
                self.main.saveTempFile()



    def keyPressEvent(self, event):
        """
        Gestion des touches Control et Delete
        """
        if event.key() == QtCore.Qt.Key_Control:
            utils_instruments.changeTracingEnabled(True)
            self.main.compass.setOpacity(1)
        self.main.keyPressEvent(event)
            
    def keyReleaseEvent(self, event):
        """
        Gestion de la touche Control
        """
        if event.key() == QtCore.Qt.Key_Control:
            utils_instruments.changeTracingEnabled(False)
            self.main.compass.setOpacity(0.5)
        self.main.keyReleaseEvent(event)



    def initLine(self, startPoint):
        """
        Début d'une ligne (un segment).
        On initialise le path et le pathItem
        """
        self.state['lastPoint'] = startPoint
        self.path = QtGui.QPainterPath()
        self.path.moveTo(startPoint)
        pen = self.drawPen
        brush = QtGui.QBrush(QtCore.Qt.green, QtCore.Qt.NoBrush)
        self.pathItem = self.scene().addPath(self.path, pen, brush)
        utils.doFlags(self.pathItem, 'ignoresTransformations')

    def drawLineTo(self, endPoint):
        """
        On supprime systématiquement la ligne précédente
            avant d'ajouter la nouvelle
        """
        self.path.lineTo(endPoint)
        if self.last != None:
            self.scene().removeItem(self.last)
        self.last = self.scene().addLine(
            self.state['lastPoint'].x(), self.state['lastPoint'].y(), 
            endPoint.x(), endPoint.y(), 
            self.drawPen)
        return self.last

    def initCurve(self, startPoint):
        """
        Début d'une courbe.
        On initialise le path et le pathItem. En cours de tracé, 
        on affichera une suite de segments (polyligne).
        Le path sera recalculé à la fin du tracé, pour tracer une courbe.
        On récupèrera donc les points de passage dans une liste (listPoints).
        """
        self.state['lastPoint'] = startPoint
        self.path = QtGui.QPainterPath()
        self.path.moveTo(startPoint)
        if self.main.drawingMode == 'HIGHLIGHTER':
            color = self.drawPen.color()
            color.setAlpha(155)
            width = self.drawPen.width()
            pen = QtGui.QPen(
                color, 
                width * 5, 
                QtCore.Qt.SolidLine, 
                QtCore.Qt.RoundCap, 
                QtCore.Qt.RoundJoin)
        else:
            pen = self.drawPen
        brush = QtGui.QBrush(QtCore.Qt.green, QtCore.Qt.NoBrush)
        self.pathItem = self.scene().addPath(self.path, pen, brush)
        utils.doFlags(self.pathItem, 'ignoresTransformations')
        # pour faire la courbe finale :
        self.listPoints = []
        self.listPoints.append(startPoint)

    def drawCurveTo(self, endPoint, endLine=False):
        """
        En cours de tracé, c'est path qui est affiché.
            On récupère le point dans listPoints.
        À la fin (appel avec la variable endLine à True), 
            on calcule la courbe, et on l'affiche.
        """
        self.path.lineTo(endPoint)
        self.state['lastPoint'] = endPoint
        self.pathItem.setPath(self.path)
        self.listPoints.append(endPoint)
        if endLine:
            """
            # tentative d'interpolation (désactivé car moche)
            self.state['lastPoint'] = endPoint
            self.path = QtGui.QPainterPath()
            self.path.moveTo(self.listPoints[0])
            for i in range(len(self.listPoints) // 2):
                self.path.quadTo(
                    self.listPoints[2*i], self.listPoints[2*i + 1])
            self.pathItem.setPath(self.path)
            """
            return self.pathItem

    def initArc(self, center, startPoint):
        """
        Début d'un arc de compas.
        On initialise les différentes valeurs d'angles utiles.
        On calcule le rectangle d'affichage 
        (carré contenant le cercle complet).
        """
        self.state['lastPoint'] = startPoint
        self.path = QtGui.QPainterPath()
        self.path.moveTo(startPoint)
        pen = self.drawPen
        brush = QtGui.QBrush(QtCore.Qt.green, QtCore.Qt.NoBrush)
        self.pathItem = self.scene().addPath(self.path, pen, brush)
        self.startPoint = startPoint
        rayon = QtCore.QLineF(center, startPoint)
        self.startAngle = rayon.angle()
        self.minAngle = self.startAngle
        self.maxAngle = self.startAngle
        self.lastAngle = self.startAngle
        length = rayon.length()
        self.rectangle = QtCore.QRectF(
            center.x() - length, center.y() 
            - length, 2 * length, 2 * length)

    def drawArcTo(self, center, endPoint):
        """
        En cours de tracé, on recalcule les différents angles utiles.
        Pas mal de cas à gérer, et la complication 
        du passage brutal de 0 à 360.
        Ensuite on trace l'arc.
        """
        self.state['lastPoint'] = QtCore.QPointF(endPoint)
        rayon = QtCore.QLineF(center, endPoint)
        self.path = QtGui.QPainterPath()
        angle = rayon.angle()
        if angle - self.lastAngle > 300:
            self.minAngle = self.minAngle + 360.0
            self.maxAngle = self.maxAngle + 360.0
        if angle - self.lastAngle < -300:
            self.minAngle = self.minAngle - 360.0
            self.maxAngle = self.maxAngle - 360.0
        if angle < self.minAngle:
            self.minAngle = angle
            self.startPoint = self.state['lastPoint']
        if angle > self.maxAngle:
            self.maxAngle = angle
        endAngle = self.maxAngle - self.minAngle
        self.path.moveTo(self.startPoint)
        self.path.arcTo(self.rectangle, self.minAngle, endAngle)
        self.pathItem.setPath(self.path)
        self.lastAngle = angle
        return self.pathItem



    def selectColor(self):
        """
        on a sélectionné une couleur prédéfinie.
        On met à jour le bouton et configDict
        """
        color = self.sender().data()
        self.drawPen.setColor(color)
        self.main.toolsWindow.actionColors.setIcon(
            self.sender().icon())
        self.main.configDict['COLORS'][
            'actualColor'] = self.sender().objectName()

    def editColor(self):
        """
        modification d'une couleur prédéfinie.
        Après validation, on met à jour la couleur, 
        le bouton et configDict
        """
        try:
            self.main.toolsWindow.hide()
            color = self.drawPen.color()
            newColor = QtWidgets.QColorDialog.getColor(
                color, 
                self.main, 
                QtWidgets.QApplication.translate('main', 'Select color'), 
                QtWidgets.QColorDialog.ShowAlphaChannel)
            if newColor.isValid():
                self.drawPen.setColor(newColor)
                pixmap = QtGui.QPixmap(
                    utils.STYLE['PM_LargeIconSize'], 
                    utils.STYLE['PM_LargeIconSize'])
                pixmap.fill(newColor)
                icon = QtGui.QIcon(pixmap)
                self.main.toolsWindow.actionColors.setIcon(icon)
                actionName = self.main.configDict['COLORS']['actualColor']
                for action in self.main.toolsWindow.actionCustomColors:
                    if action.objectName() == actionName:
                        action.setIcon(icon)
                        action.setData(newColor)
                        self.main.configDict['COLORS'][action.objectName()] = (
                            newColor.red(), 
                            newColor.green(), 
                            newColor.blue(), 
                            newColor.alpha())
        finally:
            self.main.toolsWindow.show()



    def selectWidth(self):
        """
        on a sélectionné une épaisseur prédéfinie.
        On met à jour le bouton et configDict
        """
        penWidth = self.sender().data()
        self.drawPen.setWidth(penWidth)
        self.main.toolsWindow.actionWidths.setIcon(
            self.sender().icon())
        textEdit = QtWidgets.QApplication.translate(
            'main', 'click to edit')
        text = utils_functions.u('{0} {1} ({2})').format(
            QtWidgets.QApplication.translate('main', 'Width:'), 
            penWidth, 
            textEdit)
        self.main.toolsWindow.actionWidths.setText(text)
        self.main.configDict['WIDTHS'][
            'actualWidth'] = self.sender().objectName()

    def editWidth(self):
        """
        modification d'une épaisseur prédéfinie.
        Après validation, on doit réorganiser la liste des épaisseurs
        pour garder l'ordre croissant.
        On met ensuite à jour l'épaisseur, le bouton et configDict
        """
        try:
            self.main.toolsWindow.hide()
            newWidth, ok = QtWidgets.QInputDialog.getInt(
                self.main, 
                utils.PROGLABEL, 
                QtWidgets.QApplication.translate('main', 'Select pen width:'), 
                self.drawPen.width(), 
                1, 100, 1)
            if ok:
                oldActionName = self.main.configDict['WIDTHS']['actualWidth']
                for action in self.main.toolsWindow.actionCustomWidths:
                    if action.objectName() == oldActionName:
                        action.setData(newWidth)
                # on réorganise la liste pour garder l'ordre croissant :
                widths = []
                for action in self.main.toolsWindow.actionCustomWidths:
                    widths.append(action.data())
                widths = sorted(widths)
                for i in range(5):
                    action = self.main.toolsWindow.actionCustomWidths[i]
                    width = widths[i]
                    action.setData(width)
                    action.setText('{0}'.format(width))
                    self.main.configDict['WIDTHS'][
                        action.objectName()] = width
                    if width == newWidth:
                        newObjectName = action.objectName()
                        newIcon = action.icon()
                # on met à jour le bouton :
                self.drawPen.setWidth(newWidth)
                self.main.toolsWindow.actionWidths.setIcon(newIcon)
                textEdit = QtWidgets.QApplication.translate(
                    'main', 'click to edit')
                text = utils_functions.u('{0} {1} ({2})').format(
                    QtWidgets.QApplication.translate('main', 'Width:'), 
                    newWidth, 
                    textEdit)
                self.main.toolsWindow.actionWidths.setText(text)
                self.main.configDict['WIDTHS']['actualWidth'] = newObjectName

        finally:
            self.main.toolsWindow.show()



    def setPenStyle(self, objectName=''):
        """
        sélection du style du crayon
        """
        styles = {
            'Solid': (
                self.main.toolsWindow.actionPenStyleSolid, 
                QtCore.Qt.SolidLine), 
            'Dash': (
                self.main.toolsWindow.actionPenStyleDash, 
                QtCore.Qt.DashLine), 
            'Dot': (
                self.main.toolsWindow.actionPenStyleDot, 
                QtCore.Qt.DotLine), 
            'DashDot': (
                self.main.toolsWindow.actionPenStyleDashDot, 
                QtCore.Qt.DashDotLine), 
            'DashDotDot': (
                self.main.toolsWindow.actionPenStyleDashDotDot, 
                QtCore.Qt.DashDotDotLine), 
            }
        if objectName in styles:
            (sender, style) = styles[objectName]
        else:
            sender = self.sender()
            objectName = sender.objectName()
            style = styles[objectName][1]
        self.main.toolsWindow.actionPenStyle.setText(sender.text())
        self.main.toolsWindow.actionPenStyle.setIcon(sender.icon())

        self.drawPen.setStyle(style)
        self.main.configDict['PEN']['style'] = objectName



